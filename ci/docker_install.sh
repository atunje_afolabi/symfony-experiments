#!/usr/bin/env bash


# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install apt-utils -yqq

apt-get install wget zip unzip git sqlite sqlite3 libsqlite3-dev -yqq

docker-php-ext-install pdo
docker-php-ext-install pdo_mysql
docker-php-ext-install pdo_sqlite
docker-php-ext-install ctype
docker-php-ext-install iconv

# Install and run Composer
wget https://getcomposer.org/download/1.8.5/composer.phar
# php composer.phar install
COMPOSER_MEMORY_LIMIT=-1 php composer.phar update
