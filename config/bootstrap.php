<?php

use Symfony\Component\Dotenv\Dotenv;

require dirname(__DIR__).'/vendor/autoload.php';

// Load cached env vars if the .env.local.php file exists
// Run "composer dump-env prod" to create it (requires symfony/flex >=1.2)
if (is_array($env = @include dirname(__DIR__).'/.env.local.php') && ($_SERVER['APP_ENV'] ?? $_ENV['APP_ENV'] ?? $env['APP_ENV']) === $env['APP_ENV']) {
    foreach ($env as $k => $v) {
        $_ENV[$k] = $_ENV[$k] ?? (isset($_SERVER[$k]) && 0 !== strpos($k, 'HTTP_') ? $_SERVER[$k] : $v);
    }
} elseif (!class_exists(Dotenv::class)) {
    throw new RuntimeException('Please run "composer require symfony/dotenv" to load the ".env" files configuring the application.');
} else {
    // load all the .env files
    (new Dotenv(false))->loadEnv(dirname(__DIR__).'/.env');
}

$_SERVER += $_ENV;
$_SERVER['APP_ENV'] = $_ENV['APP_ENV'] = ($_SERVER['APP_ENV'] ?? $_ENV['APP_ENV'] ?? null) ?: 'dev';
$_SERVER['APP_DEBUG'] = $_SERVER['APP_DEBUG'] ?? $_ENV['APP_DEBUG'] ?? 'prod' !== $_SERVER['APP_ENV'];
$_SERVER['APP_DEBUG'] = $_ENV['APP_DEBUG'] = (int) $_SERVER['APP_DEBUG'] || filter_var($_SERVER['APP_DEBUG'], FILTER_VALIDATE_BOOLEAN) ? '1' : '0';

// W3 ==============================================================

//if (isset($_ENV['BOOTSTRAP_CLEAR_CACHE_ENV'])) {
//    // executes the "php bin/console cache:clear" command
//    passthru(sprintf(
//        'php "%s/../bin/console" cache:clear --env=%s --no-warmup',
//        __DIR__,
//        $_ENV['BOOTSTRAP_CLEAR_CACHE_ENV']
//    ));
//}

if (isset($_ENV['BOOTSTRAP_RESET_DATABASE']) && $_ENV['BOOTSTRAP_RESET_DATABASE'] == true) {

    // Drop database if it exists
    passthru('php bin/console doctrine:database:drop --force --if-exists --env=test --no-interaction');

    // Create test database
    passthru('php bin/console doctrine:database:create --env=test --no-interaction');

    // Execute migrations to setup database before running tests
    passthru('php bin/console doctrine:migrations:migrate --env=test --no-interaction');

//    echo " Done" . PHP_EOL . PHP_EOL;
}