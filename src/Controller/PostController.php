<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PostController extends AbstractController
{
    /**
     * Create a new user
     *
     * @Route("/posts", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function createUser(Request $request)
    {
        $user = $request->request->all();

        $user = new User();
        $user->setName("Ben Adam")
            ->setAddress("100 tannin Street");

        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        return $this->json($user);
    }

    /**
     * Get all posts
     *
     * @param EntityManagerInterface $em
     * @return JsonResponse
     * @Route("/posts", methods={"GET"})
     */
    public function getPosts(EntityManagerInterface $em)
    {
//        $em->getFilters()->disable('softdeleteable');
        $posts = $this->getDoctrine()->getRepository(Post::class)->findAll();

        return $this->json($posts, 200, [], $this->circleReference());
    }

    /**
     * The post will be soft-deleted
     *
     * @param $id
     *
     * @param EntityManagerInterface $em
     * @Route("/posts/{id}", methods={"DELETE"})
     * @return JsonResponse
     */
    public function deletePost($id, EntityManagerInterface $em)
    {
        $post = $this->getDoctrine()->getRepository(Post::class)->findOneBy(['id' => $id]);

        if(!$post){
            return $this->json("Post does not exist");
        }

        $em->remove($post);
        $em->flush();

        return $this->json("Post deleted");
    }

    /**
     * @param $id
     *
     * @param EntityManagerInterface $em
     * @Route("/users/{id}", methods={"DELETE"})
     * @return JsonResponse
     */
    public function deleteUser($id, EntityManagerInterface $em)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);

        if (!$user) {
            throw $this->createNotFoundException("User not found");
        }

        $em->remove($user);
        $em->flush();

        return $this->json("User deleted");
    }

    /**
     * Update a post
     *
     * @param $id
     *
     * @Route("/post/{id}", methods={"PUT"})
     * @return JsonResponse
     */
    public function update($id)
    {
        $post = $this->getDoctrine()->getRepository(Post::class)->findOneBy(['id' => $id]);

        $post->setTitle('A NEW title')
            ->setBody('A NEW body');

        $this->getDoctrine()->getManager()->flush();

        return $this->json("Post updated successfully");
    }

    /**
     * @param $id
     *
     * @param EntityManagerInterface $em
     * @Route("/users/{id}/restore", methods={"PATCH"})
     * @return JsonResponse
     */
    public function restoreUser($id, EntityManagerInterface $em)
    {
        $em->getFilters()->disable('softdeleteable');

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);

        if(!$user){
            return $this->json("User not found");
        }

        $user->setDeletedAt(null);

//        dd($user->getPost()->get(0));
//        foreach($user->getPost() as $obj) {
//            dd($obj);
//        }

        $em->flush();

        return $this->json("User restored...");
    }

    private function circleReference()
    {
        return [
            ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            }
        ];
    }
}
