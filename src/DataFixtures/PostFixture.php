<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PostFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setAddress("90 Ololade street")
            ->setName("Alade Olowosokedile");

        $post = new Post();
        $post->setBody("Grand post body")
            ->setTitle("Grand post title")
            ->setUser($user);
        $manager->persist($post);

        $post2 = new Post();
        $post2->setBody("Example body...Cool beans")
            ->setTitle("Cool beans")
            ->setUser($user);
        $manager->persist($post2);

        $manager->flush();
    }
}
