<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\DataFixtures\PostFixture;

class PostControllerTest extends WebTestCase
{
    private $client;
    private $entityManager;

    public function setUp() : void
    {
        $this->client = static::createClient();
        $this->entityManager = $this->client->getContainer()->get('doctrine')->getManager();
    }

    public function testCreateUser()
    {
        // define User data array
        $userData = [
            'name' => 'Ben Adam',
            'address' => '100 Tannin Street'
        ];

        // Hit create user endpoint
        $client = static::createClient();
        $client->request('POST', '/posts', $userData);

        $userObjectResponse = json_decode($client->getResponse()->getContent());

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertSame('Ben Adam', $userObjectResponse->name);
    }

    public function testGetPosts()
    {
        // Use fixture to put 2 posts into database
        $this->loadFixtures();

        // Hit endpoint to get all posts
        $this->client->request('GET', '/posts');
        $response = json_decode($this->client->getResponse()->getContent());

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->client->getResponse()->headers->contains('Content-Type', 'application/json'));
        $this->assertCount(2, $response);
    }

    private function loadFixtures(): void
    {
        $fixture = new PostFixture();
        $fixture->load($this->entityManager);
    }
}

